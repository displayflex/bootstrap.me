#! /bin/bash

PACKAGES=(
  vim_config
)

# Debug via
# export GIT_SSH_COMMAND='ssh -Tv'
# Temp measuere is ssh creds unused otherwise
# export GIT_SSH_COMMAND='ssh -i ~/.ssh/id_displayflex -o IdentitiesOnly=yes'

# Needed for vim_config's loading of config partials
mkdir -p /home/docker/Projects/util/
cd /home/docker/Projects/util/

wget "https://bitbucket.org/displayflex/bootstrap.me/raw/master/install_git_packages.sh" -O install_git_packages.sh
chmod +x ./install_git_packages.sh
. ./install_git_packages.sh git@bitbucket.org:displayflex
