FROM ruby:2.6.0

RUN mkdir /app
RUN apt-get update \
  && apt-get install -y --no-install-recommends sudo git ruby curl net-tools ca-certificates openssh-client software-properties-common \
  && rm -rf /var/lib/apt/lists/*

RUN useradd -m docker && echo "docker:d" | chpasswd && adduser docker sudo
USER docker
RUN mkdir -p /home/docker/.ssh/; ssh-keyscan github.com >> /home/docker/.ssh/known_hosts; ssh-keyscan bitbucket.org >> /home/docker/.ssh/known_hosts
COPY ssh/config /home/docker/.ssh/config

WORKDIR /app

ENTRYPOINT ["/bin/bash"]
