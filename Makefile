
build:
	docker build -t bootstrap_me .

sh: build
	docker run -it --rm -v $$PWD:/app -v /Users/law/.ssh/:/home/docker/.ssh/ bootstrap_me
