#! /bin/bash

for PACKAGE in "${PACKAGES[@]}"
do
  git clone -vvv $1/$PACKAGE.git
  cd $PACKAGE
  ./install.sh || ./run.sh
  cd ..
  echo -e "Installed ${PACKAGE} from install_git_packages \n\n"
done
